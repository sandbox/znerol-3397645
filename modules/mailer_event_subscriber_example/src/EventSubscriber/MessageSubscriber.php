<?php

namespace drupal\mailer_event_subscriber_example\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Event\FailedMessageEvent;
use Symfony\Component\Mailer\Event\MessageEvent;
use Symfony\Component\Mailer\Event\SentMessageEvent;
use Symfony\Component\Mime\Email;

/**
 * Example mailer transport event subscriber.
 */
class MessageSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a new mailer transport event subscriber.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel to use.
   */
  public function __construct(protected LoggerInterface $logger) {
  }

  /**
   * Acts on the message before the body is rendered.
   */
  public function onMessageBeforeBodyRenderer(MessageEvent $event) {
    // No example yet.
  }

  /**
   * Acts on the message after the body is rendered.
   *
   * Adds additional Cc addresses to any message.
   */
  public function onMessageAfterBodyRenderer(MessageEvent $event) {
    $message = $event->getMessage();
    if ($message instanceof Email) {
      $message->addCc('additional-cc@example.com', 'Another Copy <another-cc@example.com>');
      $this->logger->info('Added additional cc to message');
    }
    else {
      $this->logger->error('Attempted to add additional cc to message, but object has unexpected type @type', [
        '@type' => get_class($message),
      ]);
    }
  }

  /**
   * Fires when a message was sent successfully.
   */
  public function onSentMessage(SentMessageEvent $event) {
    $message = $event->getMessage();
    $this->logger->info('Successfully sent message with id @id', [
      '@id' => $message->getMessageId(),
    ]);
  }

  /**
   * Fires when a message failed to send successfully.
   */
  public function onFailedMessage(FailedMessageEvent $event) {
    $this->logger->error($event->getError());
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[MessageEvent::class][] = ['onMessageBeforeBodyRenderer', 31];
    $events[MessageEvent::class][] = ['onMessageAfterBodyRenderer', -32];
    $events[SentMessageEvent::class][] = ['onSentMessage', 0];
    $events[FailedMessageEvent::class][] = ['onFailedMessage', 0];
    return $events;
  }

}
