<?php

declare(strict_types=1);

namespace Drupal\mailer_transport_from_env_example;

use Drupal\Core\Mailer\TransportServiceFactoryInterface;
use Symfony\Component\DependencyInjection\Attribute\AutowireDecorated;
use Symfony\Component\DependencyInjection\Attribute\AutowireIterator;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mailer\Transport\TransportInterface;

/**
 * Transport service factory which honors the MAILER_DSN environment variable.
 */
class EnvTransportServiceFactory implements TransportServiceFactoryInterface {

  protected Transport $transportFacade;

  /**
   * Constructs a new env transport service factory.
   *
   * @param \Drupal\Core\Mailer\TransportServiceFactoryInterface $inner
   *   The decorated transport service factory.
   * @param Iterable<TransportFactoryInterface> $factories
   *   A list of transport factories.
   */
  public function __construct(
    #[AutowireDecorated]
    protected TransportServiceFactoryInterface $inner,
    #[AutowireIterator(tag: 'mailer.transport_factory')]
    iterable $factories,
  ) {
    $this->transportFacade = new Transport($factories);
  }

  /**
   * {@inheritdoc}
   */
  public function createTransport(): TransportInterface {
    $envMailerDsn = getenv("MAILER_DSN");
    if (!empty($envMailerDsn)) {
      return $this->transportFacade->fromString($envMailerDsn);
    }
    else {
      return $this->inner->createTransport();
    }
  }

}
