<?php

namespace Drupal\mailer_form_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Mailer\Exception\RuntimeException;
use Symfony\Component\Mailer\Header\TagHeader;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * Mailer test form.
 */
class MailerTestForm extends FormBase implements FormInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get(MailerInterface::class));
  }

  /**
   * Constructs the mailer test form.
   *
   * @param \Symfony\Component\Mailer\MailerInterface $mailer
   *   The mailer.
   */
  public function __construct(protected MailerInterface $mailer) {
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailer_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['tag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mail id tag'),
      '#description' => $this->t('A tag added to the email which can be used by event subscribers and third-party transports for grouping, tracking and workflows (<a href="https://symfony.com/doc/current/mailer.html#adding-tags-and-metadata-to-emails">Docs</a>)'),
      '#default_value' => 'mailer_form_example_test_mail',
    ];

    $form['count'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of mails'),
      '#description' => $this->t('Send this number of emails (test roundrobin transport)'),
      '#default_value' => 1,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => 'Send Mail',
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tag = $form_state->getValue('tag');
    $count = $form_state->getValue('count');

    for ($i = 0; $i < $count; $i++) {
      $email = new Email();
      $email->subject("Test message {$i}")
        ->from('test@localhost.localdomain')
        ->text('Hello test runner!');

      if (!empty($tag)) {
        $email->getHeaders()->add(new TagHeader($tag));
      }

      try {
        $this->mailer->send($email->to('admin@localhost.localdomain'));
        $this->messenger()->addStatus($this->t('Sent test message'));
      }
      catch (RuntimeException $e) {
        $this->messenger()->addError($this->t('Failed to send test message'));
        break;
      }
    }
  }

}
