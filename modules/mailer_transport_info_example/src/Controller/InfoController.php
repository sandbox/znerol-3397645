<?php

namespace Drupal\mailer_transport_info_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\mailer_transport_info_example\TransportFactoryInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Transport factory info controller.
 */
class InfoController extends ControllerBase implements ContainerInjectionInterface {

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(TransportFactoryInfo::class)
    );
  }

  /**
   * Constructs new transport factory info controller.
   */
  public function __construct(protected TransportFactoryInfo $factoryInfo) {
  }

  /**
   * Generates info page about registered transport factories.
   */
  public function infoPage(): array {
    $build['factory_table'] = [
      '#theme' => 'table',
      '#header' => [
        'id' => ['data' => $this->t('Service Id')],
        'class' => ['data' => $this->t('Class')],
        'priority' => ['data' => $this->t('Priority')],
      ],
    ];

    foreach ($this->factoryInfo->getTransportFactoryInfo() as ['id' => $serviceId, 'class' => $class, 'priority' => $priority]) {
      $build['factory_table']['#rows'][] = [$serviceId, $class, $priority];
    }

    return $build;
  }

}
