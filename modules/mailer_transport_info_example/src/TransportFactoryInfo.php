<?php

namespace Drupal\mailer_transport_info_example;

use Symfony\Component\Mailer\Transport\TransportFactoryInterface;

/**
 * Collects info about registered transport factories.
 */
class TransportFactoryInfo {

  /**
   * An unsorted array of arrays of transport factory info records.
   *
   * An associative array. The keys are integers that indicate priority. Values
   * are arrays of transport factory info records.
   *
   * @var \Symfony\Component\Mailer\Transport\TransportFactoryInterface[][]
   */
  protected array $transportFactories;

  /**
   * Registers a transport factory.
   *
   * @param \Symfony\Component\Mailer\Transport\TransportFactoryInterface $transportFactory
   *   A transport factory.
   * @param string $id
   *   The service id.
   * @param int $priority
   *   The priority of the transport factory being added.
   */
  public function addTransportFactory(TransportFactoryInterface $transportFactory, string $id, ?int $priority = 0): void {
    $this->transportFactories[$priority][] = [
      'id' => $id,
      'class' => get_class($transportFactory),
      'priority' => $priority,
    ];
  }

  /**
   * Return info about registered transport factories.
   *
   * @return array
   */
  public function getTransportFactoryInfo(): array {
    krsort($this->transportFactories);
    return array_merge(...$this->transportFactories);
  }

}
