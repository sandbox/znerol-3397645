<?php

namespace Drupal\mailer_transport_factory_example\Transport;

use Symfony\Component\Mailer\Exception\UnsupportedSchemeException;
use Symfony\Component\Mailer\Transport\AbstractTransportFactory;
use Symfony\Component\Mailer\Transport\Dsn;
use Symfony\Component\Mailer\Transport\TransportFactoryInterface;
use Symfony\Component\Mailer\Transport\TransportInterface;

/**
 * An example transport factory for the logger transport.
 */
class LoggerTransportFactory extends AbstractTransportFactory implements TransportFactoryInterface {

  /**
   * {@inheritdoc}
   */
  protected function getSupportedSchemes(): array {
    return ['drupal-example.logger'];
  }

  /**
   * {@inheritdoc}
   */
  public function create(Dsn $dsn): TransportInterface {
    if ($dsn->getScheme() === 'drupal-example.logger') {
      return new LoggerTransport($this->dispatcher, $this->logger);
    }

    throw new UnsupportedSchemeException($dsn, 'example_logger', $this->getSupportedSchemes());
  }

}
