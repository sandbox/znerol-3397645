<?php

namespace Drupal\mailer_transport_factory_example\Transport;

use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport\AbstractTransport;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Symfony\Component\Mime\Message;

/**
 * An example transport which just logs the message without delivering it.
 */
class LoggerTransport extends AbstractTransport implements TransportInterface {

  /**
   * {@inheritdoc}
   */
  protected function doSend(SentMessage $message): void {
    $email = $message->getOriginalMessage();
    $id = $message->getMessageId();
    if ($email instanceof Message) {
      $headers = $email->getHeaders();
      $this->getLogger()->info('Message @id from: %from, to: %to, cc: %cc, subject: %subject. Logged only, no delivery.', [
        '@id' => $id,
        '%from' => $headers->get('From')?->getBodyAsString() ?: 'none',
        '%to' => $headers->get('To')?->getBodyAsString() ?: 'none',
        '%cc' => $headers->get('Cc')?->getBodyAsString() ?: 'none',
        '%subject' => $headers->get('Subject')?->getBodyAsString() ?: 'none',
      ]);
    }
    else {
      $this->getLogger()->error('Failed to handle message @id, object has unexpected type @type', [
        '@id' => $id,
        '@type' => get_class($email),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return 'drupal-example.logger://default';
  }

}
