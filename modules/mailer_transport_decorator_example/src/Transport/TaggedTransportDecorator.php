<?php

namespace Drupal\mailer_transport_decorator_example\Transport;

use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\Header\TagHeader;
use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Symfony\Component\Mime\Message;
use Symfony\Component\Mime\RawMessage;

/**
 * Decorator for the mailer.transport service which matches tags on emails to
 * transport ids.
 */
class TaggedTransportDecorator implements TransportInterface {

  /**
   * Constructs a new tagged transport decorator.
   *
   * @param \Symfony\Component\Mailer\Transport\TransportInterface $inner
   *   The inner transport where all calls are forwarded after processing email
   *   tags.
   * @param iterable<string, string> $tagMap
   *   A list of key value pairs where the key is an fnmatch() tag pattern and
   *   the value is a transport id.
   */
  public function __construct(
    protected TransportInterface $inner,
    protected iterable $tagMap
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function send(RawMessage $message, ?Envelope $envelope = NULL): ?SentMessage {
    if ($message instanceof Message && !$message->getHeaders()->has('X-Transport')) {
      $transportId = $this->getTransportId($message);
      if ($transportId !== NULL) {
        $message->getHeaders()->addHeader('X-Transport', $transportId);
      }
    }

    return $this->inner->send($message, $envelope);
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return (string) $this->inner;
  }

  /**
   * Returns transportId matching the tag header of the given message.
   *
   * @param \Symfony\Component\Mime\Message $message
   *   The message.
   *
   * @return string|null
   *   The transport id or null if none was found.
   */
  protected function getTransportId(Message $message): ?string {
    foreach ($message->getHeaders()->all() as $header) {
      if ($header instanceof TagHeader) {
        foreach ($this->tagMap as $pattern => $transportId) {
          if (fnmatch($pattern, $header->getValue())) {
            return $transportId;
          }
        }
      }
    }

    return NULL;
  }

}
