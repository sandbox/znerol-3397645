<?php

namespace Drupal\mailer_transport_decorator_example\Transport;

use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Symfony\Component\Mailer\Transport\Transports;
use Symfony\Component\Mime\RawMessage;

/**
 * Decorator for the mailer.transport service which allows to specify multiple
 * transports.
 */
class MultiTransportDecorator implements TransportInterface {

  /**
   * All transports.
   */
  protected Transports $transports;

  /**
   * Constructs a new multi transport decorator.
   *
   * @param \Symfony\Component\Mailer\Transport\TransportInterface $defaultTransport
   *   The default transport to use when the message doesn't specify a specific
   *   transport.
   * @param iterable<string, \Symfony\Component\Mailer\Transport\TransportInterface> $transportMap
   *   A list of key value pairs where the key is the id of a multi transport
   *   config entity and the value is a corresponding transport implementation.
   */
  public function __construct(
    TransportInterface $defaultTransport,
    array $transportMap,
  ) {
    $this->transports = new Transports([
      'default' => $defaultTransport,
      ...$transportMap,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function send(RawMessage $message, ?Envelope $envelope = NULL): ?SentMessage {
    return $this->transports->send($message, $envelope);
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return (string) $this->transports;
  }

}
