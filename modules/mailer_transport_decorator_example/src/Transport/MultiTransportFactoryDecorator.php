<?php

namespace Drupal\mailer_transport_decorator_example\Transport;

use Drupal\Core\Mailer\TransportServiceFactoryInterface;
use Drupal\Core\Mailer\TransportServiceFactoryTrait;
use Drupal\mailer_transport_decorator_example\Entity\TransportInstance;
use Symfony\Component\DependencyInjection\Attribute\AutowireDecorated;
use Symfony\Component\DependencyInjection\Attribute\AutowireIterator;
use Symfony\Component\Mailer\Transport\Dsn;
use Symfony\Component\Mailer\Transport\TransportInterface;

/**
 * The example transport factory adapter decorator.
 *
 * Adapts the Symfony mailer transport factory class to better suit decoration
 * of an existing transport with config entity based multi transport.
 *
 * @see \Symfony\Component\Mailer\Transport
 */
class MultiTransportFactoryDecorator implements TransportServiceFactoryInterface {

  use TransportServiceFactoryTrait;

  /**
   * Constructs a new example transport factory decorator.
   *
   * @param \Drupal\Core\Mailer\TransportServiceFactoryInterface $innerFactory
   *   The inner configured transport factory.
   * @param Iterable<TransportFactoryInterface> $factories
   *   A list of transport factories.
   */
  public function __construct(
    #[AutowireDecorated]
    protected TransportServiceFactoryInterface $innerFactory,
    #[AutowireIterator(tag: 'mailer.transport_factory')]
    iterable $factories
  ) {
    $this->factories = $factories;
  }

  /**
   * {@inheritdoc}
   */
  public function createTransport(): TransportInterface {
    $innerTransport = $this->innerFactory->createTransport();

    $transportTags = [];
    $transports = [];
    foreach (TransportInstance::loadMultiple() as $instance) {
      $id = $instance->id();
      $dsnObject = new Dsn(...$instance->get('dsn'));
      $weight = $instance->get('weight') ?: 0;
      $tags = explode(' ', $instance->get('tags') ?: '');
      $transports[$weight][$id] = $this->fromDsnObject($dsnObject);
      foreach (array_filter($tags) as $tag) {
        $transportTags[$weight][$tag] = $id;
      }
    }
    ksort($transports);
    $orderedTransports = array_merge(...$transports);
    $inner = new MultiTransportDecorator($innerTransport, $orderedTransports);

    krsort($transportTags);
    $reversedTagMap = array_merge(...$transportTags);
    $orderedTagMap = array_reverse($reversedTagMap, TRUE);
    return new TaggedTransportDecorator($inner, $orderedTagMap);
  }

}
