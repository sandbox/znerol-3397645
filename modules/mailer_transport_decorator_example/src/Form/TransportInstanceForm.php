<?php

declare(strict_types = 1);

namespace Drupal\mailer_transport_decorator_example\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mailer_transport_decorator_example\Entity\TransportInstance;

/**
 * Transport Instance form.
 */
final class TransportInstanceForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [TransportInstance::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
    ];

    $dsn = $this->entity->get('dsn');
    $form['dsn'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('DSN'),
    ];
    $form['dsn']['scheme'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scheme'),
      '#default_value' => $dsn['scheme'],
      '#required' => TRUE,
    ];
    $form['dsn']['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#default_value' => $dsn['host'],
      '#required' => TRUE,
    ];
    $form['dsn']['port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#default_value' => $dsn['port'],
    ];
    $form['dsn']['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $dsn['user'],
    ];
    $form['dsn']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $dsn['password'],
    ];

    $form['tags'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tags'),
      '#default_value' => $this->entity->get('tags'),
      '#description' => $this->t('Space separated list of email tags this instance will act on.'),
    ];

    $form['weight'] = [
      '#type' => 'number',
      '#title' => $this->t('Weight'),
      '#default_value' => $this->entity->get('weight') ?: 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new example %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated example %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
