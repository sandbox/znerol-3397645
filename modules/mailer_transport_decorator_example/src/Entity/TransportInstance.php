<?php

declare(strict_types = 1);

namespace Drupal\mailer_transport_decorator_example\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\mailer_transport_decorator_example\TransportInstanceInterface;

/**
 * Defines the transport instance entity type.
 *
 * @ConfigEntityType(
 *   id = "transport_instance",
 *   label = @Translation("Transport Instance"),
 *   label_collection = @Translation("Transport Instances"),
 *   label_singular = @Translation("transport instance"),
 *   label_plural = @Translation("transport instances"),
 *   label_count = @PluralTranslation(
 *     singular = "@count transport instance",
 *     plural = "@count transport instances",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\mailer_transport_decorator_example\TransportInstanceListBuilder",
 *     "form" = {
 *       "add" = "Drupal\mailer_transport_decorator_example\Form\TransportInstanceForm",
 *       "edit" = "Drupal\mailer_transport_decorator_example\Form\TransportInstanceForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "transport_instance",
 *   admin_permission = "administer transport_instance",
 *   links = {
 *     "collection" = "/admin/structure/transport-instance",
 *     "add-form" = "/admin/structure/transport-instance/add",
 *     "edit-form" = "/admin/structure/transport-instance/{transport_instance}",
 *     "delete-form" = "/admin/structure/transport-instance/{transport_instance}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "dsn",
 *     "tags",
 *     "weight",
 *   },
 * )
 */
final class TransportInstance extends ConfigEntityBase implements TransportInstanceInterface {

  /**
   * The transport instance ID.
   */
  protected string $id;

  /**
   * The transport instance label.
   */
  protected string $label;

  /**
   * The transport instance description.
   */
  protected string $description;

  /**
   * The transport instance DSN.
   */
  protected array $dsn;

  /**
   * Space separated list of email tags this instance will act on.
   */
  protected string $tags;

  /**
   * The weight of this transport instance.
   */
  protected int $weight;

}
