<?php

declare(strict_types = 1);

namespace Drupal\mailer_transport_decorator_example;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a transport instance entity type.
 */
interface TransportInstanceInterface extends ConfigEntityInterface {
}
