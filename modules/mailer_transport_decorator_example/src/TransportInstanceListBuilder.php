<?php declare(strict_types = 1);

namespace Drupal\mailer_transport_decorator_example;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of transport instances.
 */
final class TransportInstanceListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    $header['tags'] = $this->t('Tags');
    $header['weight'] = $this->t('Weight');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\mailer_transport_decorator_example\TransportInstanceInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    $row['tags'] = $entity->get('tags');
    $row['weight'] = $entity->get('weight');
    return $row + parent::buildRow($entity);
  }

}
